import React from "react";
import List from "../../components/ListItem";
const Homepage = () => {
  const myList = [
    "herbata z miodem",
    "kawa z mniszka",
    "woda",
    "koktajl owocowy",
  ];
  return (
    <div>
      <h3>Moje ulubione napoje:</h3>
      <ul>
        {myList.map((el, index) => {
          return <List key={index} name={el} />;
        })}
      </ul>
    </div>
  );
};

export default Homepage;
