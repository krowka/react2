import React from "react";
const Header = (props) => {
  return (
    <div>
      <h1>To jest kurs react.js, witam Cię {props.name}</h1>
      <p> A to jest test własny</p>
    </div>
  );
};
// class Header extends React.Component {
//   render() {
//     return (
//       <div>
//         <h1>To jest kurs react.js2, witam Cię {this.props.name}</h1>
//       </div>
//     );
//   }
// }
export default Header;
